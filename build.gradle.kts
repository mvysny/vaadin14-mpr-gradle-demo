import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.gradle.api.tasks.testing.logging.TestExceptionFormat

val mprVersion = "2.0.5"
val karibuTestingVersion = "1.2.5"
val vaadin14Version = "14.3.7"
val karibuDslVersion = "1.0.3"

configurations {
    compile.isCanBeResolved = true // workaround for Vaadin 8 plugin
    runtime.isCanBeResolved = true // workaround for Vaadin 8 plugin
}

plugins {
    kotlin("jvm") version "1.9.21"
    // need to use Gretty here because of https://github.com/johndevs/gradle-vaadin-plugin/issues/317
    id("org.gretty") version "3.0.6"
    // you need two Vaadin plugins to build this project correctly:
    // This plugin builds the Vaadin 8 part (the widgetset/GWT)
    id("com.devsoap.plugin.vaadin") version "2.0.0.beta2"  // https://plugins.gradle.org/plugin/com.devsoap.plugin.vaadin
    // This plugin builds the Vaadin 14 part (npm).
    id("com.vaadin") version "0.14.3.7"
}

defaultTasks("clean", "build")

repositories {
    mavenCentral()
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

// this block configures the Vaadin 8 Vaadin plugin (com.devsoap.plugin.vaadin)
vaadin {
    version = "8.14.1"
}

gretty {
    contextPath = "/"
    servletContainer = "jetty9.4"
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        // to see the exceptions of failed tests in Travis-CI console.
        exceptionFormat = TestExceptionFormat.FULL
    }
}

dependencies {
    // don't use "api" or "implementation" dependencies otherwise the WAR
    // file will not contain vaadin jars.

    // Karibu-DSL dependency
    compile("com.github.mvysny.karibudsl:karibu-dsl-v8:$karibuDslVersion")
    compile("com.github.mvysny.karibudsl:karibu-dsl:$karibuDslVersion")

    // MPR + Vaadin 14 dependency
    compile("com.vaadin:vaadin-core:$vaadin14Version") {
        // Webjars are only needed when running in Vaadin 13 compatibility mode
        listOf("com.vaadin.webjar", "org.webjars.bowergithub.insites",
            "org.webjars.bowergithub.polymer", "org.webjars.bowergithub.polymerelements",
            "org.webjars.bowergithub.vaadin", "org.webjars.bowergithub.webcomponents")
                .forEach { group -> exclude(group = group) }
    }
    compile("com.vaadin:mpr-v8:$mprVersion")

    // workaround for https://github.com/johndevs/gradle-vaadin-plugin/issues/536
    vaadinCompile("com.vaadin:mpr-core:$mprVersion") {
        // removes duplicate dependencies: https://gitlab.com/mvysny/vaadin14-mpr-gradle-demo/issues/1
        exclude(group = "com.vaadin")
        exclude(group = "commons-io")
    }

    // include proper kotlin version
    compile(kotlin("stdlib-jdk8"))

    // logging
    // currently we are logging through the SLF4J API to SLF4J-Simple. See src/main/resources/simplelogger.properties file for the logger configuration
    compile("org.slf4j:slf4j-simple:2.0.13")
    // this will allow us to configure Vaadin to log to SLF4J
    compile("org.slf4j:jul-to-slf4j:2.0.13")

    // test support
    testImplementation("com.github.mvysny.kaributesting:karibu-testing-v8:$karibuTestingVersion")
    testImplementation("com.github.mvysny.kaributesting:karibu-testing-v10:$karibuTestingVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.11.0")

    // workaround until https://youtrack.jetbrains.com/issue/IDEA-178071 is fixed
    compile("com.vaadin:vaadin-themes:${vaadin.version}")
}

// This block configures the Vaadin 14 plugin.
// Typically Vaadin 14 plugin is configured via a vaadin{} block, but that would
// conflict with Vaadin 8 plugin (which is also configured via a vaadin{} block).
// In such cases, Vaadin 14 plugin avoids this kind of conflicts by allowing the configuration
// via a vaadin14{} block.
vaadin14 {
    pnpmEnable = true
}

afterEvaluate {
    if (vaadin14.productionMode) {
        dependencies {
            // this also enables production mode for Vaadin 8. See
            // https://gitlab.com/mvysny/vaadin7-14-mpr-gradle-demo/-/issues/1
            // for more details.
            compile("com.vaadin:flow-server-production-mode:2.3.5")
        }
    }
}
