package org.test

import com.github.mvysny.karibudsl.v10.h1
import com.github.mvysny.karibudsl.v8.*
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.router.Route
import com.vaadin.flow.server.VaadinService
import com.vaadin.mpr.LegacyWrapper
import com.vaadin.mpr.MprUI
import com.vaadin.mpr.core.LegacyUI
import com.vaadin.mpr.core.MprTheme
import com.vaadin.mpr.core.MprWidgetset
import com.vaadin.server.VaadinRequest
import com.vaadin.ui.Composite
import com.vaadin.ui.HasComponents
import server.mycomponent.MyComponent

class OldForm : Composite() {
    private val root = verticalLayout {
        val name = textField("Type your name here:")
        myComponent()
        button("Click Me") {
            onLeftClick {
                println("Thanks ${name.value}, it works!")
                this@verticalLayout.label("Thanks ${name.value}, it works!")
            }
        }
    }
}

@VaadinDsl
fun (@VaadinDsl HasComponents).myComponent(block: (@VaadinDsl MyComponent).() -> Unit = {}) = init(MyComponent(), block)

@Route("")
@MprWidgetset("AppWidgetset")
@MprTheme("mytheme")
@LegacyUI(MyUI::class)
class AddressbookLayout : Div() {
    init {
        h1("Welcome to Vaadin 14!")
        add(LegacyWrapper(OldForm()))

        println("Running in production mode: Vaadin 14: ${VaadinService.getCurrent().deploymentConfiguration.isProductionMode}, Vaadin 7: ${com.vaadin.server.VaadinService.getCurrent()?.deploymentConfiguration?.isProductionMode}")
    }
}

class MyUI : MprUI() {
    override fun init(request: VaadinRequest?) {
        super.init(request)
        // initialize the old UI
        println("Old UI initialized")
    }
}
