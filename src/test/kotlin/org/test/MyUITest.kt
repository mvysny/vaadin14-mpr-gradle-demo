package org.test

import com.github.mvysny.kaributesting.v8.*
import com.github.mvysny.kaributesting.mockhttp.MockHttpSession
import com.github.mvysny.kaributesting.v10.Routes
import com.vaadin.flow.component.html.H1
import com.vaadin.flow.server.VaadinSession
import com.vaadin.flow.server.WrappedHttpSession
import com.vaadin.mpr.LegacyWrapper
import com.vaadin.mpr.MprUI
import com.vaadin.mpr.core.AbstractMprUIContent
import com.vaadin.server.VaadinRequest
import com.vaadin.ui.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import server.mycomponent.MyComponent
import kotlin.test.expect

/**
 * Tests the UI. Uses the Browserless testing approach as provided by the [Karibu Testing](https://github.com/mvysny/karibu-testing) library.
 */
class MyUITest {
    @BeforeEach fun fakeVaadin() {
        // bootstrap Vaadin 14 first
        com.github.mvysny.kaributesting.v10.MockVaadin.setup(Routes().autoDiscoverViews("org.test"))

        // Vaadin 8 init follows:
        // 1. make sure that Vaadin 8 has the same session than Vaadin 14. Get Vaadin 14's session first.
        val session: MockHttpSession = (VaadinSession.getCurrent().session as WrappedHttpSession).httpSession as MockHttpSession

        // 2. make sure that MPR can access Vaadin 8 stuff via AbstractMprUIContent
        val uiContents: Map<String, Any?> = session.attributeNames.toList()
                .associateWith { session.getAttribute(it) }
                .filter { it.value is AbstractMprUIContent }
        expect(1, "Found ${uiContents.size} AbstractMprUIContent in session: $uiContents") { uiContents.size }
        val windowName: String = uiContents.keys.first()

        // 3. Finally, mock Vaadin 8
        MockVaadin.setup({
            val request: VaadinRequest = VaadinRequest.getCurrent()
            request.parameterMap["v-wn"] = arrayOf(windowName)
            MprUI()
        }, session, session.servletContext)
    }
    @AfterEach fun tearDownVaadin() {
        MockVaadin.tearDown()
        com.github.mvysny.kaributesting.v10.MockVaadin.tearDown()
    }

    @Test fun `Vaadin 8 Form`() {
        // Test Vaadin 8. Since the components are technically nested inside of UI.getCurrent(), we
        // can use global lookup functions to find Vaadin components.

        // Simulate a text entry as if entered by the user
        _get<TextField> { caption = "Type your name here:" }._value = "Baron Vladimir Harkonnen"

        // simulate a button click as if clicked by the user
        _get<Button> { caption = "Click Me" }._click()

        _expectOne<MyComponent>()

        // verify that there is a single Label and assert on its value
        expect("Thanks Baron Vladimir Harkonnen, it works!") { _get<Label>().value }
    }

    @Test fun `Vaadin 14 component lookup`() {
        com.github.mvysny.kaributesting.v10._expectOne<H1> { text = "Welcome to Vaadin 14!" }
    }

    @Test fun `Vaadin 14 LegacyWrapper lookup & legacy component unwrap`() {
        // Test lookup of the wrapper component from Vaadin 14 UI
        val wrapper = com.github.mvysny.kaributesting.v10._get<LegacyWrapper>()

        // Test unwrapping the Vaadin 8 component from the Vaadin 14 Wrapper component.
        val oldForm = wrapper.legacyComponent as OldForm
        _get<Button> { caption = "Click Me" }._click()
    }
}
